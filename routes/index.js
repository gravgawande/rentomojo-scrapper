var express = require('express');
var router = express.Router();
var ScrapeService = require('../services/scrape-service');

/* GET home page. */
router.get('/', function(req, res, next) {
	ScrapeService.scrapeWeb();
  res.render('index', { title: 'Express' });
  // ScrapeService.scrapeWeb();
});

module.exports = router;

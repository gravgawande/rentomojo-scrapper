var URL = 'https://medium.com/';
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var csvWriter = require('csv-write-stream');
var writer = csvWriter();
var async = require('async');
var dataArray = [];
var DATALENGTH ;
(function(){

function _scrapeWeb (){


        _createTasks(function(err, resp){
            if(resp){
                _scrapeWeb();
            }else{  
                console.log('scraping done')
            }
        });

}

function _createTasks(_callback){
                
                if(dataArray.length == 0){
                    DATALENGTH = 0;
                    url = URL;
                     _getFileLinks(url , function (err, resp) {
                            if(err){
                                console.log(err)
                            }else{
                                _createTasks();
                            }
                        });
                }else{

                if(DATALENGTH < dataArray.length){
                    var tasks  = [];
                    for (var i = DATALENGTH ; i < dataArray.length; i++) {
                        if(typeof dataArray[i] != 'undefined'){
                            var reqUrl = dataArray[i];
                            var task = function(_cb){
                                _getFileLinks(reqUrl , function (err, resp) {
                                    if(err){
                                        console.log('err');
                                        _cb(err);
                                    }else{
                                        _cb(null , resp);
                                    }
                                });
                            }
                          tasks.push(task);   
                        }
                    };
                    DATALENGTH = dataArray.length;
                    async.parallelLimit(tasks, 5 , function(err, resp){

                        if(err){
                            console.log(err);
                        }else{
                            _createTasks();
                        }
                    }); 
                }else{
                    console.log('sending to write');
                    _writeCSV(function(err, resp){
                        if(err){

                        }else{
                            console.log('done');
                            console.log('file created')
                           return _callback(null , true);
                        }
                    });
                    
                }
            }
        // });
}

function _getFileLinks (param, _cb) {
    request(param, function(error, response, html){
        if(error){
           return _cb(error);
        }
        else{
            var $ = cheerio.load(html);
            var title, release, rating;
            var data = $('a[data-action-source]');
            var tasks = [];
            for (var i = 0; i < data.length; i++) {
                dataArray.push( data[i].attribs.href);
            };
            _cb(null , true);
        }
    });
}

function _writeCSV(_cb){
    writer.pipe(fs.createWriteStream('scraped.csv'))
    for (var i = 0; i < dataArray.length; i++) {
            writer.write({link: dataArray[i]});
        };
    writer.end();

    _cb(null , true);
}


module.exports = {
	scrapeWeb : _scrapeWeb
}

})();


